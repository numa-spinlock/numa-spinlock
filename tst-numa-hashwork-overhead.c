#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include <stdint.h>
#include <stddef.h>
#include <atomic.h>
#include "numa-spinlock.h"

struct numa_spinlock *lock;

struct SHA1_CTX;

extern void SHA1_Update (struct SHA1_CTX *, const uint8_t *,
			 const size_t);

struct work_todo_argument
{
  struct SHA1_CTX *v1;
  char *v2;
  int v3;

};

static void *
work_todo (void *v)
{
  struct work_todo_argument *p = v;
  SHA1_Update (p->v1, (uint8_t *)p->v2, p->v3);
  return NULL;
}

static inline void
do_work (struct numa_spinlock_info *lock_info)
{
  numa_spinlock_apply (lock_info);
  while (numa_spinlock_pending (lock_info))
    atomic_spin_nop ();
}

#define USE_NUMA_SPINLOCK
#include "tst-hashwork-overhead-skeleton.c"
