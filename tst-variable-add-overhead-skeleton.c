/* Test case skeleton for spinlock overhead.
   Copyright (C) 2019 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

/* Check spinlock overhead with large number threads.  Critical region is
   very smmall.  Critical region + spinlock overhead aren't noticeable
   when number of threads is small.  When thread number increases,
   spinlock overhead become the bottleneck.  It shows up in wall time
   of thread execution.  */

#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include <config.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/time.h>
#include <sys/param.h>
#include <errno.h>
#ifdef MODULE_NAME
# include <cpu-features.h>
# include <support/test-driver.h>

# undef attribute_hidden
# define attribute_hidden
#endif
#include <hp-timing.h>
#include <atomic.h>

#ifndef USE_PTHREAD_ATTR_SETAFFINITY_NP
# define USE_PTHREAD_ATTR_SETAFFINITY_NP 1
#endif

#define CACHELINE_SIZE	64
#define CACHE_ALIGNED	__attribute__((aligned(CACHELINE_SIZE)))

#define constant_time 5
unsigned long long g_value CACHE_ALIGNED;
struct count
{
  unsigned long long total;
  unsigned long long spinlock;
  unsigned long long wall;
} __attribute__((aligned(128)));

struct count *gcount;

/* The time consumed by one update is about 200 TSCs.  */
static int delay_time_unlocked = 400;

struct ops
{
  void *(*test) (void *arg);
  void (*print_thread) (void *res, int);
} *ops;

struct stats_result
{
  unsigned long num;
};

void *work_thread (void *arg);

#define iterations (10000 * 5)

static volatile int start_thread;

/* Delay some fixed time */
static void
delay_tsc (unsigned n)
{
#if HP_TIMING_AVAIL
  hp_timing_t start, current, diff;
  HP_TIMING_NOW (start);

  while (1)
    {
      HP_TIMING_NOW (current);
      HP_TIMING_DIFF (diff, start, current);
      if (diff < n)
	atomic_spin_nop ();
      else
	break;
    }
#endif
}

static void
wait_a_bit (int delay_time)
{
  if (delay_time > 0)
    delay_tsc (delay_time);
}

#ifndef USE_NUMA_SPINLOCK
static inline void
work_todo (void)
{
  __sync_fetch_and_add(&g_value, 1);
}
#endif

void *
work_thread (void *arg)
{
  long i;
  unsigned long pid = (unsigned long) arg;
  struct stats_result *res;
  int err_ret = posix_memalign ((void **)&res, CACHELINE_SIZE,
				roundup (sizeof (*res), CACHELINE_SIZE));
  if (err_ret)
    {
      printf ("posix_memalign failure: %s\n", strerror (err_ret));
      exit (err_ret);
    }
  long num = 0;

#ifdef USE_NUMA_SPINLOCK
  struct work_todo_argument work_todo_arg;
  struct numa_spinlock_info lock_info;

  if (numa_spinlock_init (lock, &lock_info))
    {
      printf ("numa_spinlock_init failure: %m\n");
      exit (1);
    }

  work_todo_arg.v1 = &g_value;
  lock_info.argument = &work_todo_arg;
  lock_info.workload = work_todo;
#endif

  while (!start_thread)
    atomic_spin_nop ();

#if HP_TIMING_AVAIL
  hp_timing_t start, end;
  HP_TIMING_NOW (start);
#endif

  for (i = 0; i < iterations; i++)
    {
#ifdef USE_NUMA_SPINLOCK
      do_work (&lock_info);
#else
      do_work ();
#endif
      wait_a_bit (delay_time_unlocked);
      num++;
    }
#if HP_TIMING_AVAIL
  HP_TIMING_NOW (end);
  HP_TIMING_DIFF (gcount[pid].total, start, end);
#endif
  res->num = num;

  return res;
}

void
init_global_data(void)
{
  g_value = 0;
}

void
test_threads (int numthreads, int numprocs, unsigned long time)
{
  start_thread = 0;

#ifdef USE_NUMA_SPINLOCK
  lock = numa_spinlock_alloc ();
#endif

  atomic_full_barrier ();

  pthread_t thr[numthreads];
  void *res[numthreads];
  int i;

  init_global_data ();
  for (i = 0; i < numthreads; i++)
    {
      pthread_attr_t attr;
      const pthread_attr_t *attrp = NULL;
      if (USE_PTHREAD_ATTR_SETAFFINITY_NP)
	{
	  attrp = &attr;
	  pthread_attr_init (&attr);
	  cpu_set_t set;
	  CPU_ZERO (&set);
	  int cpu = i % numprocs;
	  (void) CPU_SET (cpu, &set);
	  pthread_attr_setaffinity_np (&attr, sizeof (cpu_set_t), &set);
	}
      int err_ret = pthread_create (&thr[i], attrp, ops->test,
				    (void *)(uintptr_t) i);
      if (err_ret != 0)
	{
	  printf ("pthread_create failed: %d, %s\n",
		  i, strerror (i));
	  numthreads = i;
	  break;
	}
    }

  atomic_full_barrier ();
  start_thread = 1;
  atomic_full_barrier ();
  sched_yield ();

  if (time)
    {
      struct timespec ts =
	{
	  ts.tv_sec = time,
	  ts.tv_nsec = 0
	};
      clock_nanosleep (CLOCK_MONOTONIC, 0, &ts, NULL);
      atomic_full_barrier ();
    }

  for (i = 0; i < numthreads; i++)
    {
      if (pthread_join (thr[i], (void *) &res[i]) == 0)
	free (res[i]);
      else
	printf ("pthread_join failure: %m\n");
    }

#ifdef USE_NUMA_SPINLOCK
  numa_spinlock_free (lock);
#endif
}

struct ops hashwork_ops =
{
  .test = work_thread,
};

struct ops *ops;

static struct count
total_cost (int numthreads, int numprocs)
{
  int i;
  unsigned long long total = 0;
  unsigned long long spinlock = 0;

  memset (gcount, 0, sizeof(gcount[0]) * numthreads);

#if HP_TIMING_AVAIL
  hp_timing_t start, end, diff;
  HP_TIMING_NOW (start);
#endif

  test_threads (numthreads, numprocs, constant_time);

#if HP_TIMING_AVAIL
  HP_TIMING_NOW (end);
  HP_TIMING_DIFF (diff, start, end);
#endif

  for (i = 0; i < numthreads; i++)
    {
      total += gcount[i].total;
      spinlock += gcount[i].spinlock;
    }

  struct count cost = { total, spinlock, diff };
  return cost;
}

#ifdef MODULE_NAME
static int
do_test (void)
{
# if !HP_TIMING_AVAIL
  return EXIT_UNSUPPORTED;
# endif
#else
int
main (void)
{
#endif
  int numprocs = sysconf (_SC_NPROCESSORS_ONLN);

  /* Oversubscribe CPU.  */
  int numthreads = 4 * numprocs;

  ops = &hashwork_ops;

  int err_ret = posix_memalign ((void **)&gcount, 4096,
				sizeof(gcount[0]) * numthreads);
  if (err_ret)
    {
      printf ("posix_memalign failure: %s\n", strerror (err_ret));
      exit (err_ret);
    }

  struct count cost, cost1;
  double overhead;
  int i, last;
  int last_increment = numprocs < 16 ? 16 : numprocs;
  int numprocs_done = 0;
  int numprocs_reset = 0;
  cost1 = total_cost (1, numprocs);

  printf ("Number of processors: %d, Single thread time %lld\n\n",
	  numprocs, cost1.total);

  for (last = i = 2; i <= numthreads;)
    {
      last = i;
      cost = total_cost (i, numprocs);
      overhead = cost.total;
      overhead /= i;
      overhead /= cost1.total;
      printf ("Number of threads: %4d, Total time %14lld, Overhead: %.2f\n",
	      i, cost.total, overhead);
      if ((i * 2) < numprocs)
	i = i * 2;
      else if (numprocs_done)
	{
	  if (numprocs_reset)
	    {
	      i = numprocs_reset;
	      numprocs_reset = 0;
	    }
	  else
	    {
	      if ((i * 2) < numthreads)
		i = i * 2;
	      else
		i = i + last_increment;
	    }
	}
      else
	{
	  if (numprocs != 2 * i)
	    numprocs_reset = 2 * i;
	  i = numprocs;
	  numprocs_done = 1;
	}
    }

  if (last != numthreads)
    {
      i = numthreads;
      cost = total_cost (i, numprocs);
      overhead = cost.total;
      overhead /= i;
      overhead /= cost1.total;
      printf ("Number of threads: %4d, Total time %14lld, Overhead: %.2f\n",
	      i, cost.total, overhead);
    }

  free (gcount);
  return 0;
}

#ifdef MODULE_NAME
# define TIMEOUT 900
# include <support/test-driver.c>
#endif
