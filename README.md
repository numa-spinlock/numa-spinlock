# numa-spinlock

To build the NUMA spinlock library, do

1. ./configure
2. make

This will build both libnuma-spinlock as well as test workloads.

Configure options:

1. --enable-affinity.  Use pthread_attr_setaffinity_np in workloads.
This is enabled by default.
