#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include <mcs-spinlock.h>

struct
{
  mcs_spinlock_t testlock;
  char pad[64 - sizeof (mcs_spinlock_t)];
} test __attribute__((aligned(64)));

static void
__attribute__((constructor))
init_spin (void)
{
  mcs_spin_init (&test.testlock, 0);
}

static void work_todo (void);

static inline void
do_work (void)
{
  mcs_spin_lock(&test.testlock);
  work_todo ();
  mcs_spin_unlock(&test.testlock);
}

#include "tst-variable-share-overhead-skeleton.c"
