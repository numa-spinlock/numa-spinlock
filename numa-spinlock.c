/* NUMA spinlock
   Copyright (C) 2019 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#include <config.h>
#include <string.h>
#include <stdlib.h>
#include <sched.h>
#ifndef HAVE_GETCPU
#include <unistd.h>
#include <syscall.h>
#endif
#include <errno.h>
#include <atomic.h>
#include "numa-spinlock-private.h"

#if !defined HAVE_GETCPU && defined _LIBC
# define HAVE_GETCPU
#endif

/* On multi-socket systems, memory is shared across the entire system.
   Data access to the local socket is much faster than the remote socket
   and data access to the local core is faster than sibling cores on the
   same socket.  For serialized workloads with conventional spinlock,
   when there is high spinlock contention between threads, lock ping-pong
   among sockets becomes the bottleneck and threads spend majority of
   their time in spinlock overhead.

   On multi-socket systems, the keys to our NUMA spinlock performance
   are to minimize cross-socket traffic as well as localize the serialized
   workload to one core for execution.  The basic principles of NUMA
   spinlock are mainly consisted of following approaches, which reduce
   data movement and accelerate critical section, eventually give us
   significant performance improvement.

   1. MCS spinlock
   MCS spinlock help us to reduce the useless lock movement in the
   spinning state.  This paper provides a good description for this
   kind of lock:
   <http://www.cise.ufl.edu/tr/DOC/REP-1992-71.pdf>

   2. Critical Section Integration (CSI)
   Essentially spinlock is similar to that one core complete critical
   sections one by one. So when contention happen, the serialized works
   are sent to the core who is the lock owner and responsible to execute
   them, that can save much time and power, because all shared data are
   located in private cache of the lock owner.

   We implemented this mechanism based on queued spinlock in kernel, that
   speeds up critical section, and reduces the probability of contention.
   The paper provides a good description for this kind of lock:
   <https://users.ece.cmu.edu/~omutlu/pub/acs_asplos09.pdf>

   3. NUMA Aware Spinlock (NAS)
   Currently multi-socket systems give us better performance per watt,
   however that also involves more complex synchronization requirement,
   because off-chip data movement is much slower. We use distributed
   synchronization mechanism to decrease Lock cache line to and from
   different nodes. The paper provides a good description for this kind
   of lock:
   <https://www.usenix.org/system/files/conference/atc17/atc17-kashyap.pdf>

   4. Yield Schedule
   When threads are applying for Critical Section Integration(CSI) with
   known contention, they will delegate work to the thread who is the
   lock owner, and wait for work to be completed.  The resources which
   they are using should be transferred to other threads. In order to
   accelerate the scenario, we introduce yield_sched function during
   spinning stage.

   5. Optimization when NUMA is ON or OFF.
   Although programs can access memory with lower latency when NUMA is
   enabled, some programs may need more memory bandwidth for computation
   with NUMA disabled.  We also optimize multi-socket systems with NUMA
   disabled.

   NUMA spinlock flow chart (assuming there are 2 CPU nodes):

   1. Threads from node_0/node_1 acquire local lock for node_0/1
   respectively.  If the thread succeeds in acquiring local lock, it
   goes to step 2, otherwise pushes critical function into current
   local work queue, and enters into spinning stage with MCS mode.

   2. Threads from node_0/node_1 acquire the global lock.  If it succeeds
   in acquiring the global lock as the lock owner, it goes to step 3,
   otherwise waits until the lock owner thread releases the global lock.

   3. The lock owner thread from node_0/1 enters into critical section,
   cleans up work queue by performing all local critical functions
   pushed at step 1 with CSI on behalf of other threads and informs
   those spinning threads that their works have been done.  It then
   releases the local lock.

   4. The lock owner thread frees global lock.  If another thread is
   waiting at step 2, the lock owner thread passes the global lock to
   the waiting thread and returns.  The new lock owner thread enters
   into step 3.  If no threads are waiting, the lock owner thread
   releases the global lock and returns.  The whole critical section
   process is completed.

   Steps 1 and 2 mitigate global lock contention.  Only one thread
   from different nodes will compete for the global lock in step 2.
   Step 3 reduces the global lock & shared data movement because they
   are located in the same node as well as the same core.  Our data
   shows that Critical Section Integration (CSI) improves data locality
   and NUMA-aware spinlock (NAS) helps CSI balance the workload.

   NUMA spinlock can greatly speed up critical section on multi-socket
   systems.  It should improve spinlock performance on all multi-socket
   systems.

   NOTE: LiTL <https://github.com/multicore-locks/litl>, is an open-source
   project that provides implementations of dozens of various locks,
   including several state-of-the-art NUMA-aware spinlocks.  Among them

   1. Hierarchical MCS (HMCS) spinlock.  Milind Chabbi, Michael Fagan,
   and John Mellor-Crummey. High Performance Locks for Multi-level NUMA
   Systems.  In Proceedings of the ACM SIGPLAN Symposium on Principles
   and Practice of Parallel Programming (PPoPP), pages 215–226, 2015.

   2. Cohort-MCS (C-MCS) spinlock.  Dave Dice, Virendra J. Marathe, and
   Nir Shavit.  Lock Cohorting: A General Technique for Designing NUMA
   Locks. ACM Trans. Parallel Comput., 1(2):13:1–13:42, 2015.
 */

/* Get the next thread pointed to by *NEXT_P.  NB: We must use a while
   spin loop to load NEXT_P since there is a small window before *NEXT_P
   is updated.  */

static inline struct numa_spinlock_info *
get_numa_spinlock_info_next (struct numa_spinlock_info **next_p)
{
  struct numa_spinlock_info *next;
  while (!(next = atomic_load_relaxed (next_p)))
    atomic_spin_nop ();
  return next;
}

/* While holding the global NUMA spinlock, run the workload of the
   thread pointed to by SELF first, then run the workload for each
   thread on the thread list pointed to by HEAD_P and wake up the
   thread so that all workloads run on a single processor.  */

static inline void
run_numa_spinlock (struct numa_spinlock_info *self,
		   struct numa_spinlock_info **head_p)
{
  struct numa_spinlock_info *next, *current;

  /* Run the SELF's workload. */
  self->result = self->workload (self->argument);

  /* Process workloads for the rest of threads on the thread list.
     NB: The thread list may be prepended by other threads at the
     same time.  */

retry:
   /* If SELF is the first thread of the thread list pointed to by
      HEAD_P, clear the thread list.  */
  current = atomic_compare_and_exchange_val_acq (head_p, NULL, self);
  if (current == self)
    {
      /* Since SELF is the only thread on the list, clear SELF's pending
         field and return.  */
      atomic_store_release (&current->pending, 0);
      self->pending = 0;
      return;
    }

  /* CURRENT will have the previous first thread of the thread list
     pointed to by HEAD_P and *HEAD_P will point to SELF.  */
  current = atomic_exchange_acquire (head_p, self);

  /* NB: No need to check if CURRENT == SELF here since SELF can never
     be CURRENT.  */

repeat:
  /* Get the next thread.  */
  next = get_numa_spinlock_info_next (&current->next);

  /* Run the CURRENT's workload and clear CURRENT's pending field. */
  current->result = current->workload (current->argument);
  current->pending = 0;

  /* Process the workload for each thread from CURRENT to SELF on the
     thread list.  Don't pass beyond SELF since SELF is the last thread
     on the list.  */
  if (next == self)
    goto retry;
  current = next;
  goto repeat;
}

/* Return non-zero if the NUMA spinlock is pending.  */

static inline int
numa_spinlock_pending_p (struct numa_spinlock_info *self)
{
  return atomic_load_relaxed (&self->pending);
}

/* Apply for the NUMA spinlock with the NUMA spinlock info data pointed
   to by SELF and wait for the spinlock if NONBLOCK is zero.  */

static inline void
apply_numa_spinlock (struct numa_spinlock_info *self, int nonblock)
{
  struct numa_spinlock *lock = self->lock;
  struct numa_spinlock_info *first, *next;
  struct numa_spinlock_info **head_p;

  self->next = NULL;
  /* We want the global NUMA spinlock.  */
  self->pending = 1;
  /* Select the local NUMA spinlock list by the NUMA node number.  */
  head_p = &lock->lists[self->node];
  /* FIRST will have the previous first thread of the local NUMA spinlock
     list and *HEAD_P will point to SELF.  */
  first = atomic_exchange_acquire (head_p, self);
  if (first)
    {
      /* SELF has been prepended to the thread list pointed to by
	 HEAD_P.  NB: There is a small window between updating
	 *HEAD_P and self->next.  */
      atomic_store_release (&self->next, first);
      if (!nonblock)
	{
	  /* Let other threads run first since another thread will run our
	     workload for us.  */
	  sched_yield ();
	  /* Spin until our PENDING is cleared.  */
	  while (numa_spinlock_pending_p (self))
	    atomic_spin_nop ();
	}
      return;
    }

  /* NB: Now SELF must be the only thread on the thread list pointed
     to by HEAD_P.  Since thread is always prepended to HEAD_P, we
     can use *HEAD_P == SELF to check if SELF is the only thread on
     the thread list.  */

  if (__glibc_unlikely (lock->single_node))
    {
      /* If there is only one node, there is no need for the global
         NUMA spinlock.  */
      run_numa_spinlock (self, head_p);
      return;
    }

  /* FIRST will have the previous first thread of the local NUMA spinlock
     list of threads which holds the global NUMA spinlock, which will
     point to SELF.  */
  first = atomic_exchange_acquire (&lock->owner, self);
  if (first)
    {
      /* SELF has been prepended to the thread list pointed to by
	 lock->owner.  NB: There is a small window between updating
	 *HEAD_P and first->next.  */
      atomic_store_release (&first->next, self);
      /* Spin until the list of threads which holds the global NUMA
	 spinlock clears our PENDING.  */
      while (numa_spinlock_pending_p (self))
	atomic_spin_nop ();
    }

  /* We get the global NUMA spinlock now.  Run our workload.  */
  run_numa_spinlock (self, head_p);

  /* SELF is the only thread on the list if SELF is the first thread
     of the thread list pointed to by lock->owner.  In this case, we
     simply return.  */
  if (!atomic_compare_and_exchange_bool_acq (&lock->owner, NULL, self))
    return;

  /* Wake up the next thread.  */
  next = get_numa_spinlock_info_next (&self->next);
  atomic_store_release (&next->pending, 0);
}

/* Apply and wait for the NUMA spinlock with the NUMA spinlock info data
   pointed to by SELF.  */

void
numa_spinlock_apply (struct numa_spinlock_info *self)
{
  apply_numa_spinlock (self, 0);
}

/* Apply for the non-blocking NUMA spinlock with the NUMA spinlock info
   data pointed to by SELF.  */

void
numa_spinlock_apply_nonblock (struct numa_spinlock_info *self)
{
  apply_numa_spinlock (self, 1);
}

/* Non-zero if the NUMA spinlock is pending.  */

int
numa_spinlock_pending (struct numa_spinlock_info *self)
{
  return numa_spinlock_pending_p (self);
}

/* Initialize the NUMA spinlock info data pointed to by INFO from a
   pointer to the NUMA spinlock, LOCK.  */

int
numa_spinlock_init (struct numa_spinlock *lock,
		    struct numa_spinlock_info *info)
{
  memset (info, 0, sizeof (*info));
  info->lock = lock;
  /* For single node system, use 0 as the NUMA node number.  */
  if (lock->single_node)
    return 0;
  /* NB: Use the NUMA node number from getcpu to select the local NUMA
     spinlock list.  */
  unsigned int cpu;
  unsigned int node;
#ifdef HAVE_GETCPU
  int err_ret = getcpu (&cpu, &node);
#else
  int err_ret = syscall (SYS_getcpu, &cpu, &node, NULL);
#endif
  if (err_ret)
    return err_ret;
  if (lock->physical_package_id_p)
    {
      /* Can it ever happen?  */
      if (cpu > lock->max_cpu)
	cpu = lock->max_cpu;
      /* NB: If NUMA is disabled, use physical_package_id.  */
      node = lock->physical_package_id_p[cpu];
    }
  /* Can it ever happen?  */
  if (node > lock->max_node)
    node = lock->max_node;
  info->node = node;
  return err_ret;
}

void
numa_spinlock_free (struct numa_spinlock *lock)
{
  if (lock->physical_package_id_p)
    free (lock->physical_package_id_p);
  free (lock);
}
