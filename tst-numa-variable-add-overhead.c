/* Test case for NUMA spinlock overhead.
   Copyright (C) 2019 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include "numa-spinlock.h"

struct numa_spinlock *lock;

struct work_todo_argument
{
  unsigned long long *v1;
};

static void *
work_todo (void *v)
{
  struct work_todo_argument *p = v;
  __sync_fetch_and_add(p->v1, 1);
  return 0;
}

static inline void
do_work (struct numa_spinlock_info *lock_info)
{
  numa_spinlock_apply (lock_info);
}

#define USE_NUMA_SPINLOCK
#include "tst-variable-add-overhead-skeleton.c"
