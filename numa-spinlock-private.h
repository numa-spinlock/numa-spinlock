/* Internal definitions and declarations for NUMA spinlock.
   Copyright (C) 2019 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#include "numa-spinlock.h"

/* The global NUMA spinlock.  */
struct numa_spinlock
{
  /* List of threads who owns the global NUMA spinlock.  */
  struct numa_spinlock_info *owner;
  /* The maximium NUMA node number.  */
  unsigned int max_node;
  /* Non-zero for single node system.  */
  unsigned int single_node;
  /* The maximium CPU number.  Used only when NUMA is disabled.  */
  unsigned int max_cpu;
  /* Array of physical_package_id of each core if it isn't NULL.  Used
     only when NUMA is disabled.*/
  unsigned int *physical_package_id_p;
  /* Arrays of lists of threads who are spinning for the local NUMA lock
     on NUMA nodes indexed by NUMA node number.  */
  struct numa_spinlock_info *lists[];
};
