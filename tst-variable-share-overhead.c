/* Test case for spinlock overhead.
   Copyright (C) 2019 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include <pthread.h>

struct
{
  pthread_spinlock_t testlock;
  char pad[64 - sizeof (pthread_spinlock_t)];
} test __attribute__((aligned(64)));

static void
__attribute__((constructor))
init_spin (void)
{
  pthread_spin_init (&test.testlock, 0);
}

static void work_todo (void);

static inline void
do_work (void)
{
  pthread_spin_lock(&test.testlock);
  work_todo ();
  pthread_spin_unlock(&test.testlock);
}

#include "tst-variable-share-overhead-skeleton.c"
