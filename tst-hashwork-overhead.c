#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include <stdint.h>
#include <pthread.h>

struct
{
  pthread_spinlock_t testlock;
  char pad[64 - sizeof (pthread_spinlock_t)];
} test __attribute__((aligned(64)));

static void
__attribute__((constructor))
init_spin (void)
{
  pthread_spin_init (&test.testlock, 0);
}

struct SHA1_CTX;

extern void SHA1_Update (struct SHA1_CTX *, const uint8_t *,
			 const size_t);

static inline void
do_work (struct SHA1_CTX *ctx, char *buf, const size_t len)
{
  pthread_spin_lock(&test.testlock);
  SHA1_Update (ctx, buf, len);
  pthread_spin_unlock(&test.testlock);
}

#include "tst-hashwork-overhead-skeleton.c"
