/* Initialization of NUMA spinlock.
   Copyright (C) 2019 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#include <assert.h>
#include <ctype.h>
#include <string.h>
#include <dirent.h>
#include <stdio.h>
#include <limits.h>
#ifdef _LIBC
# include <not-cancel.h>
#else
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# define __open_nocancel		open
# define __close_nocancel_nostatus	close
# define __read_nocancel		read
#endif

#include "numa-spinlock-private.h"

static char *
next_line (int fd, char *const buffer, char **cp, char **re,
	   char *const buffer_end)
{
  char *res = *cp;
  char *nl = memchr (*cp, '\n', *re - *cp);
  if (nl == NULL)
    {
      if (*cp != buffer)
	{
	  if (*re == buffer_end)
	    {
	      memmove (buffer, *cp, *re - *cp);
	      *re = buffer + (*re - *cp);
	      *cp = buffer;

	      ssize_t n = __read_nocancel (fd, *re, buffer_end - *re);
	      if (n < 0)
		return NULL;

	      *re += n;

	      nl = memchr (*cp, '\n', *re - *cp);
	      while (nl == NULL && *re == buffer_end)
		{
		  /* Truncate too long lines.  */
		  *re = buffer + 3 * (buffer_end - buffer) / 4;
		  n = __read_nocancel (fd, *re, buffer_end - *re);
		  if (n < 0)
		    return NULL;

		  nl = memchr (*re, '\n', n);
		  **re = '\n';
		  *re += n;
		}
	    }
	  else
	    nl = memchr (*cp, '\n', *re - *cp);

	  res = *cp;
	}

      if (nl == NULL)
	nl = *re - 1;
    }

  *cp = nl + 1;
  assert (*cp <= *re);

  return res == *re ? NULL : res;
}

static int
select_cpu (const struct dirent *d)
{
  /* Return 1 for "cpuXXX" where XXX are digits.  */
  if (strncmp (d->d_name, "cpu", sizeof ("cpu") - 1) == 0)
    {
      const char *p = d->d_name + 3;

      if (*p == '\0')
	return 0;

      do
	{
	  if (!isdigit (*p))
	    return 0;
	  p++;
	}
      while (*p != '\0');

      return 1;
    }
  return 0;
}

/* Allocate a NUMA spinlock and return a pointer to it.  Caller should
   call numa_spinlock_free on the NUMA spinlock pointer to free the
   memory when it is no longer needed.  */

struct numa_spinlock *
numa_spinlock_alloc (void)
{
  const size_t buffer_size = 1024;
  char buffer[buffer_size];
  char *buffer_end = buffer + buffer_size;
  char *cp = buffer_end;
  char *re = buffer_end;

  const int flags = O_RDONLY | O_CLOEXEC;
  int fd = __open_nocancel ("/sys/devices/system/node/online", flags);
  char *l;
  unsigned int max_node = 0;
  unsigned int node_count = 0;
  if (fd != -1)
    {
      l = next_line (fd, buffer, &cp, &re, buffer_end);
      if (l != NULL)
	do
	  {
	    char *endp;
	    unsigned long int n = strtoul (l, &endp, 10);
	    if (l == endp)
	      {
		node_count = 1;
		break;
	      }

	    unsigned long int m = n;
	    if (*endp == '-')
	      {
		l = endp + 1;
		m = strtoul (l, &endp, 10);
		if (l == endp)
		  {
		    node_count = 1;
		    break;
		  }
	      }

	    node_count += m - n + 1;

	    if (m >= max_node)
	      max_node = m;

	    l = endp;
	    while (l < re && isspace (*l))
	      ++l;
	  }
	while (l < re);

      __close_nocancel_nostatus (fd);
    }

  /* NB: Some NUMA nodes may not be available, if the number of NUMA
     nodes is 1, set the maximium NUMA node number to 0.  */
  if (node_count == 1)
    max_node = 0;

  unsigned int max_cpu = 0;
  unsigned int *physical_package_id_p = NULL;

  if (max_node == 0)
    {
      /* There is at least 1 node.  */
      node_count = 1;

      /* If NUMA is disabled, use physical_package_id instead.  */
      struct dirent **cpu_list;
      int nprocs = scandir ("/sys/devices/system/cpu", &cpu_list,
			    select_cpu, NULL);
      if (nprocs > 0)
	{
	  int i;
	  unsigned int *cpu_id_p;

	  /* Find the maximum CPU number.  */
	  cpu_id_p = malloc (nprocs * sizeof (unsigned int));
	  if (cpu_id_p != NULL)
	    {
	      for (i = 0; i < nprocs; i++)
		{
		  unsigned int cpu_id
		    = strtoul (cpu_list[i]->d_name + 3, NULL, 10);
		  cpu_id_p[i] = cpu_id;
		  if (cpu_id > max_cpu)
		    max_cpu = cpu_id;
		}

	      physical_package_id_p
		= malloc ((max_cpu + 1) * sizeof (unsigned int));
	      if (physical_package_id_p != NULL)
		{
		  memset (physical_package_id_p, 0,
			  ((max_cpu + 1) * sizeof (unsigned int)));

		  max_node = UINT_MAX;

		  /* Get physical_package_id.  */
		  char path[(sizeof ("/sys/devices/system/cpu")
			     + 3 * sizeof (unsigned long int)
			     + sizeof ("/topology/physical_package_id"))];
		  for (i = 0; i < nprocs; i++)
		    {
		      struct dirent *d = cpu_list[i];
		      if (snprintf (path, sizeof (path),
				    "/sys/devices/system/cpu/%s/topology/physical_package_id",
				    d->d_name) > 0)
			{
			  fd = __open_nocancel (path, flags);
			  if (fd != -1)
			    {
			      if (__read_nocancel (fd, buffer,
						   buffer_size) > 0)
				{
				  char *endp;
				  unsigned long int package_id
				    = strtoul (buffer, &endp, 10);
				  if (package_id != ULONG_MAX
				      && *buffer != '\0'
				      && (*endp == '\0' || *endp == '\n'))
				    {
				      physical_package_id_p[cpu_id_p[i]]
					= package_id;
				      if (max_node == UINT_MAX)
					{
					  /* This is the first node.  */
					  max_node = package_id;
					}
				      else if (package_id != max_node)
					{
					  /* NB: We only need to know if
					     NODE_COUNT > 1.  */
					  node_count = 2;
					  if (package_id > max_node)
					    max_node = package_id;
					}
				    }
				}
			      __close_nocancel_nostatus (fd);
			    }
			}

		      free (d);
		    }
		}

	      free (cpu_id_p);
	    }
	  else
	    {
	      for (i = 0; i < nprocs; i++)
		free (cpu_list[i]);
	    }

	  free (cpu_list);
	}
    }

  if (physical_package_id_p != NULL && node_count == 1)
    {
      /* There is only one node.  No need for physical_package_id_p.  */
      free (physical_package_id_p);
      physical_package_id_p = NULL;
      max_cpu = 0;
    }

  /* Allocate an array of struct numa_spinlock_info pointers to hold info
     for all NUMA nodes with NUMA node number from getcpu () as index.  */
  size_t size = (sizeof (struct numa_spinlock)
		 + ((max_node + 1)
		    * sizeof (struct numa_spinlock_info *)));
  struct numa_spinlock *lock = malloc (size);
  if (lock == NULL)
    return NULL;
  memset (lock, 0, size);

  lock->max_node = max_node;
  lock->single_node = node_count == 1;
  lock->max_cpu = max_cpu;
  lock->physical_package_id_p = physical_package_id_p;

  return lock;
}
